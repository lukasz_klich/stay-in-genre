(ns stay-in-genre.tune
  (:require [clj-http.client :as client]
            [clojure.data.json :as json]
            [environ.core :refer [env]]))

(def tune-url (env :tune-url))
(def tune-auth-token (env :tune-auth-token))

(defn tune-to-channel [country customer smartcard service]
  (client/post (clojure.string/join "/" [tune-url country "User/id" customer "tune"])
               {:headers {:authorization tune-auth-token}
                :query-params {:serviceIDRef service}}))
