(ns stay-in-genre.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults api-defaults]]
            [clojure.data.json :as json]
            [clojure.tools.logging :as log]
            [stay-in-genre.core :as core]))

(defroutes app-routes

  (POST "/:country/:customer/:smartcard/next-in-genre" [country customer smartcard :as request]
    (let [{:keys [current-logical-position genre]} (:params request)
          params {:country country
                  :customer customer
                  :smartcard smartcard
                  :current-logical-position current-logical-position
                  :genre genre}]
      (core/tune-in-genre core/next-logical-position params))

  (POST "/:country/:customer/:smartcard/prev-in-genre" [country customer smartcard :as request]
    (let [{:keys [current-logical-position genre]} (:params request)
          params {:country country
                  :customer customer
                  :smartcard smartcard
                  :current-logical-position current-logical-position
                  :genre genre}]
      (core/tune-in-genre core/prev-logical-position params))))

  (route/not-found "Not Found"))

(defn init []
  (log/info "Happy hacking"))

(def app
  (->
   (routes app-routes)
   (wrap-defaults api-defaults)))
