(ns stay-in-genre.schedule
  (:require [clj-time.core :as t]
            [clj-time.format :as f]
            [clj-http.client :as client]
            [clojure.data.json :as json]
            [environ.core :refer [env]]))

(def lgi-io (env :schedule-url))

(defn- get-schedule-data [endpoint query-params]
  (let [response (client/get endpoint {:query-params query-params})]
    (:data (json/read-str (response :body) :key-fn keyword))))

(defn- get-services [region query-params]
  (get-schedule-data (str lgi-io "networks/" region "/services.json") query-params))

(defn- get-broadcasts [region query-params]
  (get-schedule-data (str lgi-io "data/" region "/broadcasts.json") query-params))

(def formatter (f/formatters :date-time-no-ms))

(defn- str-date [date]
  (f/unparse formatter date))

(defn get-channels-in-genre [country genre]
  (let [now (str-date (t/now))]
    (reduce conj {}
            (map (fn [e] {(-> e :channel :logicalPosition) e})
                 (get-broadcasts country {:video.category genre
                                                   :fields "channel.entitlementCodes,channel.logicalPosition,channel.feedId,video"
                                                   :start< now
                                                   :end> now
                                                   :limit 1000})))))

(defn get-feed-id-for-channel [country name]
  (:feedId (first
            (get-services country {:name (str "\"" name "\"")
                                            :fields "feedId"}))))
