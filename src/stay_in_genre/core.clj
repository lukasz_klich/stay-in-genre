(ns stay-in-genre.core
  (:require [stay-in-genre.schedule :as schedule]
            [stay-in-genre.tune :as tune]))

(defn next-elem-using-comp [number coll comp]
  (or (first (drop-while #(comp number %) coll))
      (first coll)))

(defn next-logical-position [number coll] (next-elem-using-comp number coll >=))
(defn prev-logical-position [number coll] (next-elem-using-comp number (reverse coll) <=))

(defn tune-in-genre [next-channel-fn params]
  (let [{:keys [country genre customer smartcard current-logical-position]} params
        channels (schedule/get-channels-in-genre country genre)
        logical-positions (sort (vec (keys channels)))
        next-channel (channels (next-channel-fn (Integer/parseInt current-logical-position)
                                                logical-positions))
        feedId (schedule/get-feed-id-for-channel country (:name (:channel next-channel)))]
    (tune/tune-to-channel country customer smartcard feedId)))
