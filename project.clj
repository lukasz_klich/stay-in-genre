(defproject stay-in-genre "0.1.0-SNAPSHOT"
  :description "next/prev channel button on steroids"
  :url "https://bitbucket.org/lukasz_klich/stay-in-genre"
  :min-lein-version "2.0.0"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [compojure "1.3.2"]
                 [ring/ring-defaults "0.1.3"]
                 [clj-http "1.0.1"]
                 [clj-time "0.9.0"]
                 [environ "1.0.0"]
                 [cats "0.3.2"]
                 [org.clojure/data.json "0.2.5"]
                 [org.clojure/tools.logging "0.3.1"]
                 [ch.qos.logback/logback-classic "1.1.1"]]
  :plugins [[lein-ring "0.9.3"]
            [lein-environ "1.0.0"]]
  :lis-opts {:pid-dir "/var/local/run"
             :install-dir "/var/local/stay_in_genre"
             :init-script-install-dir "/etc/init.d"
             :jar-install-dir "/var/local/stay_in_genre"
             :redirect-output-to "/var/local/stay_in_genre/log"
             :run-uberjar? false
             :jar-args ["-p" "8080"]
             :jvm-opts ["-server"
                        "-Xms256M"
                        "-Xmx512M"
                        "-XX:MaxPermSize=128M"]}
  :ring {:handler stay-in-genre.handler/app
         :init stay-in-genre.handler/init}
  :profiles {:default [:base :system :user :provided :dev :local-dev]
             :uberjar {:aot :all}
             :dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                                  [ring-mock "0.1.5"]]}})
