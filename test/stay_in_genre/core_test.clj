(ns stay-in-genre.core-test
  (:require [clojure.test :refer :all]
            [stay-in-genre.core :refer :all]))

(deftest next-logical-position-test
  (def channels [1,3,9,13])
  (testing "Next channel should return a lowest channel, bigger than the actual one"
    (is (= (next-logical-position 1 channels) 3)))
  (testing "Next channel should return a lowest channel, bigger than the actual one ... even when it's not on a list"
    (is (= (next-logical-position 5 channels) 9)))
  (testing "Next channel should return first channel from the list when the actual channel is the last one"
    (is (= (next-logical-position 13 channels) 1)))
  (testing "Next channel should return first channel from the list when the actual channel is greater than the last one on the list ..."
    (is (= (next-logical-position 19 channels) 1))))

(deftest prev-logical-position-test
  (def channels [1,3,9,13])
  (testing "Prev channel should return a biggest channel, lower than the actual one"
    (is (= (prev-logical-position 3 channels) 1)))
  (testing "Prev channel should return a biggest channel, lower than the actual one ... even when it's not on a list"
    (is (= (prev-logical-position 5 channels) 3)))
  (testing "Prev channel should return last channel from the list when the actual channel is the first one"
    (is (= (prev-logical-position 1 channels) 13)))
  (testing "Prev channel should return last channel from the list when the actual channel is lower than the first one on the list ..."
    (is (= (prev-logical-position 0 channels) 13))))
